Package: proxirr
Type: Package
Title: Alpha and Beta Proximity to Irreplaceability
Date: 2024-01-25
Version: 0.5
Authors@R:
  person(given = "Daniele",
         family = "Baisero",
         role = c("aut", "cre", "cph"),
         email = "daniele.baisero@gmail.com",
         comment = c(ORCID = "0000-0002-1266-7174"))
Description: Functions to measure Alpha and Beta Proximity to Irreplaceability.
  The methods for Alpha and Beta irreplaceability were first described in:
    Baisero D., Schuster R. & Plumptre A.J.
    Redefining and Mapping Global Irreplaceability.
    Conservation Biology, 2022, 36(2).
    <doi:10.1111/cobi.13806>.
License: MIT + file LICENSE
Encoding: UTF-8
Suggests: 
    testthat (>= 3.0.0),
    utils
RoxygenNote: 7.3.1
Config/testthat/edition: 3
